const path = require('path');
// 清理檔案
const CleanWebpackPlugin = require('clean-webpack-plugin');
// 壓縮javascript
const uglify = require('uglifyjs-webpack-plugin');

var cssuglify = new uglify({
    compress: {     //压缩代码
        dead_code: true,    //移除没被引用的代码
        warnings: false,     //当删除没有用处的代码时，显示警告
        loops: true //当do、while 、 for循环的判断条件可以确定是，对其进行优化
    },
    except: ['$super', '$', 'exports', 'require']    //混淆,并排除关键字
});
// 使用 extract text webpack plugin
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var extractPlugin = new ExtractTextPlugin({
    // filename: '../css/main.css', // scss轉css後另存的目標檔名
    filename: '../css/main.css', // scss轉css後另存的目標檔名
});

// 複製文件
var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: './app/index.js',
    devServer: {
        contentBase: path.join(__dirname),
        // contentBase: path.join(__dirname, "dist"),
        compress: true,
        port: 9000,
        hot: true,
        inline: true
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        extractPlugin,
        cssuglify,
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.scss$/,
                use: extractPlugin.extract({ 
                    use: [
                        'css-loader', 
                        // 'css-loader?minimize', 
                        'sass-loader',
                    ]
                }),
                
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8000,
                            name: 'images/[name].[ext]',
                            outputPath: "images/"
                        },
                    }
                ]
            },
            {
                test: /.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                use: {
                loader: "file-loader",
                    options: {
                        name: "fonts/[name].[ext]",
                        // outputPath: "fonts/"
                    }
                }
            }
        ]
    },
   
    output: {
        filename: 'bundle.js',
        // path: path.resolve(__dirname, 'dist'),
        path: path.resolve(__dirname, 'dist/js/'),
        publicPath: 'dist/'
    },    
    watch: true
};