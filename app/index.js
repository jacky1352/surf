import css from '../scss/main.scss';
// import Vue from 'vue';

jQuery(function ($) {
    // hamburger
    $('.menu-hamburger').click(function() {
        $('.mobile-menu-toggle').slideToggle();
        $(this).toggleClass('open');
    });   

    // menu 捲動
    $('.desktop-menu .menu a, .mobile-menu-toggle a').click(function () {
        $('html, body').stop().animate({
            scrollTop: $($(this).attr('href')).offset().top - 140
        }, 1000);
        return false;
    });
    
    // sticky
    var $header_desktop = $('.menu-content');
    var $nav_desktop = $('.menu-content');
    var distance_desktop = $('.menu-content').height();
    $(document).on('scroll', function () {
        var offset = $(this).scrollTop();
        // Scrolled past
        if (offset > distance_desktop) {
            $header_desktop.removeClass('container').addClass('container-fluid');
            document.getElementById('logo').src='../html-template/images/logo-black.png';
            $header_desktop.addClass('sticky-top');
        } else {
            $header_desktop.removeClass('sticky-top');
            document.getElementById('logo').src='../html-template/images/logo.png';
            $header_desktop.removeClass('container-fluid').addClass('container');
        }
        if ($(window).width() < 992){
            $('.menu-content').removeClass('container');
            $('.menu-content').addClass('container-fluid');
        }
    });
    
    //tabs
    $('.tabs-link').click(function(){
		var tab_id = $(this).attr('data-tab');
		$('.tabs-link').removeClass('active');
		$('.tabs-content').removeClass('show');
		$(this).addClass('active');
        $("#"+tab_id).addClass('show');
    });

    $(window).resize(function () {
        if ($(window).width() < 992){
            $('.menu-content').removeClass('container');
            $('.menu-content').addClass('container-fluid');
        }
    }).resize();
    
});

